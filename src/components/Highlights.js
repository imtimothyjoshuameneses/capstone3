import {Row, Col, Card, Button, Container, image, Carousel} from 'react-bootstrap';
import {NavLink, Link} from 'react-router-dom';
import myImage from '../images/profile.png';
import myImage2 from '../images/shirt2.jpg';
import myImage3 from '../images/shirt1.jpg';
import myImage4 from '../images/shorts2.png';
import myImage5 from '../images/pants1.jpg';
import myImage6 from '../images/sweater1.png';
import myImage7 from '../images/hoodie1.jpg';


export default function Highlights() {
	return (
	
		<Row> 
			<Col xs={12} md={4} className='my-2' data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000"> 
				<Card className="cardHighlight p-3 carousel-control">
				 <Carousel slide={false}>
				      <Carousel.Item>
				        <img 
				        src={myImage}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage2}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage3}
				        width = "450"
				        height = "450"
				        className="d-inline-block w-100 h-50 carousel-control"
				        />
				      </Carousel.Item>

				    </Carousel>
				    <Card.Body className="cardBody">
				        <Card.Title 
				        className="NavbarBrand-text">
				            <h2>
				            Thrift Shirts and Local Tees
				            </h2>
				        </Card.Title>

				          <Button 
				          className="" 
				          variant="warning"
						  as={NavLink}
						  to='/products'
						  >
				          Check Drips
				          </Button>
				    </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4} className='my-2' data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000"> 
				<Card className="cardHighlight p-3 carousel-control">
				<Carousel slide={false}>
				      <Carousel.Item>
				        <img 
				        src={myImage}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage4}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage5}
				        width = "450"
				        height = "450"
				        className="d-inline-block w-100 h-50 carousel-control"
				        />
				      </Carousel.Item>

				    </Carousel>
				    <Card.Body className="cardBody">
				        <Card.Title 
				        className="NavbarBrand-text">
				            <h2>
				            Thrift and Local Tee Shorts and Pants 
				            </h2>
				        </Card.Title>

				          <Button 
				          className="paraFont" 
				          variant="warning"
						  as={NavLink}
						  to='/products'>
				          Check Drips 
				          </Button>
				    </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4} className='my-2' data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000"> 
				<Card className="cardHighlight p-3 carousel-control">
				<Carousel slide={false}>
				      <Carousel.Item>
				        <img 
				        src={myImage}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage6}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage7}
				        width = "450"
				        height = "450"
				        className="d-inline-block w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				    </Carousel>
				    <Card.Body className="cardBody">
				        <Card.Title 
				        className="NavbarBrand-text">
				            <h2>
				            Thrift and Local Tee Jackets 
				            </h2>
				        </Card.Title>

				          <Button 
				          className="paraFont" 
				          variant="warning"
						  as={NavLink}
						  to='/products'>
				          Check Drips 
				          </Button>
				    </Card.Body>
				</Card>
			</Col>

			
		</Row>
	)
	
	
}