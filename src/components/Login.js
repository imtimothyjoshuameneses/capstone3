import { Form, Button, Modal } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from '../UserContext.js';
import { useModalContext } from '../ModalContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {
  // Access the 'user' state from App.js through the use of React context/provider.
  const { user, setUser } = useContext(UserContext);
  const { showLoginModal, closeModal, modalType } = useModalContext();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  function loginUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.message === 'your password is incorrect') {
          Swal.fire({
            title: 'Password is incorrect',
            text: 'Please try again',
            icon: 'warning'
          })
        } else if (result.message === `The user isn't registered yet.`) {
          Swal.fire({
            title: 'User does not exist',
            text: 'Please try again',
            icon: 'warning'
          })
        } else if (result.accessToken) {
          // Set user ID in localStorage after successful login
          localStorage.setItem('token', result.accessToken);
          localStorage.setItem('userId', result.userId);
		  localStorage.setItem('isAdmin', result.isAdmin);

          // Retrieve user details and set the global user state
          retrieveUserDetails(result.accessToken, result.userId);

          setEmail("");
          setPassword("");

          // Handle successful login, e.g., set user session
          Swal.fire({
            title: 'Logged in Success',
            text: 'You have Logged in Successfully',
            icon: 'success'
          });
		  console.log(localStorage.getItem('userId'));
		  console.log(localStorage.getItem('isAdmin'));
		  console.log(localStorage.getItem('token'));

          closeModal();
        } 
		else {
          Swal.fire({
            title: 'Something went wrong',
            text: `${email} does not exist`,
            icon: 'warning'
          })
        }
      })
      .catch((error) => {
        console.error("Login error:", error);
        alert("An error occurred during login.");
      });
  }

  const retrieveUserDetails = (token, userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`, // Use the provided token
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: userId
      })
    })
      .then(response => response.json())
      .then(result => {
		 // Verify that result contains the expected user data
		 console.log('User details result:', result);

        // Once it gets the user details, set the global user state with ID and isAdmin properties
        setUser({
          id: result._id,
          isAdmin: result.isAdmin
        });
      })
  }

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <Modal show={showLoginModal} onHide={closeModal}>
      <Form onSubmit={(event) => loginUser(event)}>
        <Modal.Header closeButton>
          <Modal.Title>Login </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              placeholder="Email"
              value={email}
              onChange={event => setEmail(event.target.value)}
              required
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={event => setPassword(event.target.value)}
              required
            />
          </Form.Group>
        </Modal.Body>

        <Modal.Footer>
          <Button variant='success' type="submit" disabled={isActive === false}> Login  </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  )
}
