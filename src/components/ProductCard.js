import {Row, Col, Card, Button, Container, image, Carousel} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState } from 'react';
import {NavLink, Link} from 'react-router-dom';


export default function ProductCard({product}){
	// Destructuring the contents of 'product'
	const {_id, name,  price, image} = product;
	// const image = "https://i.pinimg.com/564x/9b/25/ad/9b25ad0dfae5ddcba11cf323c41fce91.jpg";

	// A state is just like a variable bit with the concept of getters and setters. The getter is responsible for retrieving the current value of the state, while the setter is responsible for modifying the current value of the state. The useState() hook is responsible for setting the initial value of the state

	return(
			// <Row className="d-flex flex-column">
			<Col xs={12} md={3} className="m-3"> 
				<Card className="cardHighlight p-2 carousel-control">
				 <Card.Img variant="top" 
					src={image}
					width = "500"
			        height = "500"
			        className="ms-auto w-100 h-50 carousel-control"
				 	/>
				    <Card.Body className="cardBody">
				        <Card.Title 
				        className="NavbarBrand-text">
				            <h2>
				            {name}
				            </h2>
				        </Card.Title>

				        <Card.Text>
				          Php: {price}
				        </Card.Text>

				          <Button 
				          className="" 
				          variant="warning"
						  as={NavLink}
						  to={`/products/${_id}`}
						  >
				          Check Details
				          </Button>
				    </Card.Body>
				</Card>
			</Col>
		// </Row>
		
		
				
	)
}

ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		image: PropTypes.string.isRequired
	})
}