import {Button, Row, Col, Container, Carousel} from 'react-bootstrap';
import myImage from '../images/BG.png';
import myImage2 from '../images/BG4.jpg';
import myImage3 from '../images/BG3.jpg';

export default function Banner() {
	return(
	
 		<Container>
	 			<Row data-aos="zoom-out" data-aos-delay="100" data-aos-duration="1000">
	 				<Col xs={12} md={12}>
	 					<Carousel slide={false}>
					      <Carousel.Item>
					      	<img 
						        src={myImage}
						        width = "2040"
						        height = "608"
						        className="d-inline-block w-100 image-fluid"
						        />
					      </Carousel.Item>

					      <Carousel.Item>
					        <img 
						        src={myImage2}
						        width = "2040"
						        height = "608"
						        className="d-inline-block w-100 image-fluid"
						        />
					      
					      </Carousel.Item>

					      <Carousel.Item>
					       <img 
						        src={myImage3}
						        width = "2040"
						        height = "608"
						        className="d-inline-block w-100 image-fluid"
						        />
					      </Carousel.Item>
				    	</Carousel>
	 				</Col>
	 			</Row>

	 			<Row data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000">
	 				<Col className="py-5 text-center">
	 					<h1 className="NavbarBrand-text"> Welcome to Thrift and Stuff </h1>
	 					<h4 className="paraFont"> Happy Thrifting ! </h4>
	 				</Col>
	 			</Row>
 		</Container>
 
	
	)
}