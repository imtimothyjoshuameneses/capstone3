import { createContext, useContext, useState } from 'react';

const ModalContext = createContext();

export const useModalContext = () => useContext(ModalContext);

export const ModalProvider = ({ children }) => {
  const [showRegisterModal, setShowRegisterModal] = useState(false);
  const [showLoginModal, setShowLoginModal] = useState(false);
  const [showAddProductModal, setShowAddProductModal] = useState(false);
  const [modalType, setModalType] = useState('');

  const openModal = (type) => {
   setModalType(type);
    if (type === 'signup') {
      setShowRegisterModal(true);
    } else if (type === 'login') {
      setShowLoginModal(true);
    } else if (type === 'addProduct') {
      setShowAddProductModal(true);
    }
  };

  const closeModal = () => {
    setShowRegisterModal(false);
    setShowLoginModal(false);
    setShowAddProductModal(false);
    setModalType('');
  };

  return (
    <ModalContext.Provider 
    value={{ 
    	showRegisterModal,
   		showLoginModal,
      showAddProductModal,
    	openModal, 
    	closeModal, 
    	modalType 
    }}
    >
      {children}
    </ModalContext.Provider>
  );
};